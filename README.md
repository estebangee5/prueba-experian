# PRUEBA DESARROLLO FRONT EXPERIAN

Ludwin Esteban Sichaca Valbuena - estebangee@hotmail.com


Este proyecto fue generado con [Angular CLI](https://github.com/angular/angular-cli) version 7.3.9.

## Development server

Ejecute ng s -o para ejecutar el servidor de desarrollo. Vaya a http: // localhost: 4200. 
La aplicación se volverá a cargar automáticamente si se realiza algun cambio en los archivos de origen.

## Code scaffolding

Ejecute  `ng generate component component-name` para generar un nuevo componente. 
También puede usar `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Ejecute `ng build` para compilar el proyecto. Los artefactos de compilación se almacenarán en el directorio `dist/`.
Usar `--prod` para una compilación de produccion.


## Estructura.

app
|
|   app.component.html
|   app.component.scss
|   app.module.ts
|   app.component.ts
|
|-- components
|-- modules
|-- styles
|-- providers

- app.component*, estos archivos son el componente principal de la primera vista, que se muestra en la aplicación

- components, en esta carpeta asignamos los componentes como Modales, Snacks, Botones, componentes. index.ts, este archivo genera todos los componentes creados para identificarlos más fácilmente.

- modules, carpeta encargada de guardar las vistas para el proyecto, el panel de control, los módulos de configuración, index.ts, este archivo genera todos los módulos creados para identificarlos más fácilmente.

- styles, Carpeta responsable de guardar los estilos globales que se aplican a los componentes _variables, este archivo es responsable de registrar los estilos generales que se aplicaran a la aplicacion.

# Guias de buenas practicas
- https://code-maze.com/angular-best-practices/
- http://getbem.com/introduction/