export class Producto {
  id: number;
  albumId: number;
  title: string;
  url: string;
  thumbnailUrl: string;  
}
