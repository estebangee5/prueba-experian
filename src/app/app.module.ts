import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule, JsonpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { ProductosComponent } from './components/productos/productos.component';
import { FiltrosComponent } from './components/filtros/filtros.component';

import { DataService } from './providers/data.service';
import { FiltroCortoComponent } from './components/filtro-corto/filtro-corto.component';
import { ProductoMiniaturaComponent } from './components/producto-miniatura/producto-miniatura.component';
import { MenuComponent } from './components/menu/menu.component';
import { BarraBusquedaComponent } from './components/barra-busqueda/barra-busqueda.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductosComponent,
    FiltrosComponent,
    FiltroCortoComponent,
    ProductoMiniaturaComponent,
    MenuComponent,
    BarraBusquedaComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    JsonpModule
  ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
