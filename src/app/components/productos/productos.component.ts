import { Component, OnInit, Input } from '@angular/core';
import { Producto } from '../../shared/producto.model';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.scss']
})
export class ProductosComponent implements OnInit {

  @Input() 
  producto: Producto[];

  constructor() { }

  ngOnInit() {
  }

}
