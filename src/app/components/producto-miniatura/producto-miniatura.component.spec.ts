import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductoMiniaturaComponent } from './producto-miniatura.component';

describe('ProductoMiniaturaComponent', () => {
  let component: ProductoMiniaturaComponent;
  let fixture: ComponentFixture<ProductoMiniaturaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductoMiniaturaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductoMiniaturaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
