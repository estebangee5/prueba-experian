import { Component, OnInit, Input } from '@angular/core';
import { Producto } from '../../shared/producto.model';

@Component({
  selector: 'app-producto-miniatura',
  templateUrl: './producto-miniatura.component.html',
  styleUrls: ['./producto-miniatura.component.scss']
})
export class ProductoMiniaturaComponent implements OnInit {

  @Input() 
  producto: Producto;

  verDetalle: boolean;

  constructor() { }

  ngOnInit() {
    this.verDetalle = false;
  }

  onProductClick() {
    this.verDetalle = !this.verDetalle;
  }
}
