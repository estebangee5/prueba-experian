import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Categoria } from '../../shared/categoria.model';

@Component({
  selector: 'app-filtros',
  templateUrl: './filtros.component.html',
  styleUrls: ['./filtros.component.scss']
})
export class FiltrosComponent implements OnInit {

  @Input()
  categorias: Categoria[];

  @Input()
  otrosFiltros: any[];

  @Input()
  priceFilters: any[];

  @Output()
  filterChange = new EventEmitter<any>();  

  sideShown = false;

  constructor() { }

  ngOnInit() {
  }

  reset(otrosFiltros) {
    this.otrosFiltros = otrosFiltros;
  }

  /** Funcion para aplicar fitros personalizados  */
  filtrosPersonalizados($event, filter, type) {
    
    console.log("filtrosPersonalizados() - filter ->",filter);
    console.log("filtrosPersonalizados() - type ->",type);

    const change = $event.target.checked ? 1 : -1;
    this.filterChange.emit({
      type: type,
      filter: filter,
      isChecked: $event.target.checked,
      change: change
    });
  }

}