import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FiltroCortoComponent } from './filtro-corto.component';

describe('FiltroCortoComponent', () => {
  let component: FiltroCortoComponent;
  let fixture: ComponentFixture<FiltroCortoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FiltroCortoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FiltroCortoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
