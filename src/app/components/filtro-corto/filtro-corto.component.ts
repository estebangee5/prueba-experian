import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filtro-corto',
  templateUrl: './filtro-corto.component.html',
  styleUrls: ['./filtro-corto.component.scss']
})
export class FiltroCortoComponent implements OnInit {

  @Input()
  filters: any[];

  @Output()
  filtroCortoChange = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  onSelectChange($event) {
    this.filtroCortoChange.emit($event.target.value);
  }

}
