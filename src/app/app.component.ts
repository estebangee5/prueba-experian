import { Component, OnInit, ViewChild } from '@angular/core';
import { Producto } from './shared/producto.model';

import { FiltrosComponent } from './components/filtros/filtros.component';
import { DataService } from './providers/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  producto: Producto[];
  listaProductos: Producto[];

  mainFilter: any;


  @ViewChild('filtrosComponent')
  filtrosComponent: FiltrosComponent;

  // @ViewChild('searchComponent')
  // searchComponent: SearchBarComponent;

  filtroCorto: any[] = [
    { name: 'Producto - Ascendente to Descendente', value: 'id' },
    { name: 'Producto - Descendente to Ascendente', value: 'id' }
  ];

  otrosFiltros: any[] = [
    { nombre: 'Todo', value: 'todo', checked: true },
    { nombre: 'Color', value: 'color', checked: false },
    { nombre: 'Tamaño', value: 'tamaño', checked: false }
  ];

  originalData: any = [];

  constructor(private dataService: DataService) { }

  ngOnInit() {

    this.obtenerCategorias();
    this.obtenerProductos();
  }

  // Funcion que se encarga de recuperar las categorias
  obtenerCategorias() {
    this.dataService.getData().then(
      data => {
        this.originalData = data;
        this.mainFilter = {
          search: '',
          categorias: this.originalData.categorias.slice(0),
          otroFiltro: this.otrosFiltros[0]
        };
      });
  };

  // Funcion que se encarga de recuperar las productos
  obtenerProductos() {
    this.dataService.getProductos().subscribe(
      data => {
        this.producto = data;
        console.log("AppComponent - obtenerProductos() - producto[] ->", this.producto)
      });
  };

  // Funcion para ordenar los prodoctos de manera ascendente o descendente
  ordenarProductos(criteria) {
    this.producto.sort((a, b) => {
      if (criteria === 'idDes') {
        return -a.id;
      } else if (criteria === 'idAsc') {
        return a.id;
      } else {
        return -1;
      }
    });
  };

}